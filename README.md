## Ruben's Lunchbox

This is my random box of one-off scripts, config files, and notes. Many of these were imported from GitHub Gists and other places. Each file should be self-contained.


### Dedication

This repo is named for [Brandon's Lunchbox], a 1989 DOS game that was my first introduction to computers. Like this repo, it had a bunch of random little games that taught me things. Now this is doing the same thing.

    ┌──────────────────────────────────────┐
    │                                      │
    │            R U B E N E R D           │
    │                ╔════╗                │
    │        ╔═══════╩════╩═══════╗        │
    │        ║    R U B E N ’S    ║        │
    │        ║  L U N C H B O X   ║        │
    │        ║                    ║        │
    │        ╚════════════════════╝        │
    │      A B C D E F G H I J K L M       │
    │      N O P Q R S T U V W X Y Z       │
    │          ’+’ or Esc to quit          │
    │                                      │
    └──────────────────────────────────────┘

[Brandon's Lunchbox]: https://www.myabandonware.com/game/brandon-s-big-lunchbox-3x6#screentabs "MyAbandonwarePage Captures and Snapshots of Brandon's Lunchbox"


### Changelog

#### 2018-08-11: [virustotal-rubenerdcom](./virustotal-rubenerdcom.json)
API output from running VirusTotal against my blog.

#### 2018-07-27: [fave-char-bingo-order.pl](./fave-char-bingo-order.pl)
My overengineered shuffler for the Favourite Character Bingo meme.

#### 2018-06-25: [apple-usbc-multiport-panic.log](./apple-usbc-multiport-panic.log)
Apple's USB-C to Multiport adaptor for the 12" MacBook isn't great. It panics constantly when monitors are connected. 

#### 2018-06-25: [xenserver-find-vm-vnc-port.md](./xenserver-find-vm-vnc-port.md)
How to find the VNC port for a specific VM on Xenserver; distinct from Xen.

